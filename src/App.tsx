import React from 'react';
import Routes from './Routes'
import styles from './App.module.scss'

function App() {
  return (
      <div className={styles.root}>
        <Routes />
      </div>
  );
}

export default App;
